
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
window.Vue = require('vue');
window.$  = require( 'jquery' );
window.dt = require( 'datatables.net' );

const app = new Vue({
    el: '#app',
    mounted: function () {
        //Hello message
        console.log("///////////////////////////");
        console.log("Hello ProcessMaker");
        console.log("///////////////////////////");

        //Implemente the users data table.
        // $('#example').DataTable( {
        //     "dom": 'rtip',
        //     "ajax": {
        //         url: '/api/users'
        //     },
        //     "columns": [
        //         {"data":"usr_username"},
        //         {"data":"usr_email"},
        //         {"data":"usr_create_date"},
        //     ]
        // } );


        $(document).ready( function () {
            $('#example').DataTable( {
            "dom": 'rtip',
            "ajax": {
                url: '/api/users'
                // url: 'https://poc-1.processmaker.net/api/1.0/workflow/users?'
                // u    rl: '{{PROCESSMAKER_URL}}/api/1.0/workflow/users'
            },
            "columns": [
                {"data":"usr_username"},
                {"data":"usr_email"},
                {"data":"usr_create_date"},
            ]
        });
        } );
    }
});
