<?php

namespace App\ProcessMaker;

use Exception;
use Cache;

class ProcessMaker
{
	private $access_token = null;

	public function __construct()
	{
		$this->access_token = Cache::get('pm_access_token');
		echo "$$$$$$";
		if (empty($this->access_token)) {
			$this->access_token = $this->connect();
			Cache::put('pm_access_token', $this->access_token, 60);
		}
	}

	private function connect()
	{
		if (empty(env('PROCESSMAKER_URL'))) {
			throw new Exception("Missing PROCESSMAKER_* environment settings.");
		}

		$curl = curl_init();
		$body = [
			"grant_type" => "password",
			"username" => env('PROCESSMAKER_USER'),
			"password" => env('PROCESSMAKER_PASSWORD'),
			"scope" => "*",
		];

		curl_setopt_array($curl, array(
			CURLOPT_PORT => env('PROCESSMAKER_PORT'),
			CURLOPT_URL => env('PROCESSMAKER_URL') . "/workflow/oauth2/token",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => json_encode($body),
			CURLOPT_HTTPHEADER => array(
				"authorization: Basic eC1wbS1sb2NhbC1jbGllbnQ6MTc5YWQ0NWM2Y2UyY2I5N2NmMTAyOWUyMTIwNDZlODE",
				"cache-control: no-cache",
				"content-type: application/json"
			),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			throw new Exception($err);
		}

		$tokens = json_decode($response, false);

		if (!$tokens) {
			throw new Exception("Unable to connect to Processmaker: $response");
		}

		if (empty($tokens->access_token)) {
			throw new Exception("Unable to get the access token: $response");
		}

		return $tokens->access_token;
	}

	public function getAccessToken()
	{
		return $this->access_token;
	}
}