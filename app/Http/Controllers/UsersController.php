<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Faker\Generator;
use App\User;
use App\ProcessMaker\ProcessMaker;

class UsersController extends Controller
{
    public function index(Request $request) {
        // $list = [];
        // for($i=0; $i < 100; $i++) {
        //     $list[]=[
        //         "usr_username" => $faker->name,
        //         "usr_email" => $faker->unique()->safeEmail,
        //         "usr_create_date" => $faker->date,
        //     ];
        // }
        // return ["data" => $list];
        
        $pmServer = env('PROCESSMAKER_URL');

        $accessToken = $this->approve();
        $list=[];

        $ch = curl_init($pmServer . "/api/1.0/workflow/users");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer " . $accessToken));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $users = json_decode(curl_exec($ch));
        $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $i = 0;
        foreach ($users as $value) {
            $list[$i] = [
                "usr_username" => $value->usr_username,
                "usr_email" => $value->usr_email,
                "usr_create_date" => $value->usr_create_date,
            ];
            $i++;
        }
        return ["data" => $list];

    }

    public function approve() {


        // $pm = new ProcessMaker();
        // $accessToken = $pm->getAccessToken();

        $curl = curl_init();
        $body = [
            "grant_type" => "password",
            "username" => env('PROCESSMAKER_USER'),
            "password" => env('PROCESSMAKER_PASSWORD'),
            "scope" => "*",
        ];

        curl_setopt_array($curl, array(
            CURLOPT_PORT => env('PROCESSMAKER_PORT'),
            CURLOPT_URL => env('PROCESSMAKER_URL') . "/workflow/oauth2/token",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($body),
            CURLOPT_HTTPHEADER => array(
                "authorization: Basic eC1wbS1sb2NhbC1jbGllbnQ6MTc5YWQ0NWM2Y2UyY2I5N2NmMTAyOWUyMTIwNDZlODE",
                "cache-control: no-cache",
                "content-type: application/json"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            throw new Exception($err);
        }

        $tokens = json_decode($response, false);

        if (!$tokens) {
            throw new Exception("Unable to connect to Processmaker: $response");
        }

        if (empty($tokens->access_token)) {
            throw new Exception("Unable to get the access token: $response");
        }
        return $tokens->access_token;
    }
}
